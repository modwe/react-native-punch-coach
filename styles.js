import { StyleSheet } from "react-native";

const red = "#f44242";
const white = "#fff";
const black = "#000";

export const MenuStyles = StyleSheet.create({
  horizontal: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignSelf:'center'
  },
  header:{
    position:'absolute',
    top:0
  },
  content: {
    backgroundColor: red
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: "center",
    flexDirection: "column"
  },
  half: {
    backgroundColor: black
  },
  listItem: {
    marginLeft: 0,
    paddingLeft: 10,
    paddingRight:10,
    flex: 1,
    flexDirection: 'row',
    alignItems:'center',
    backgroundColor: white,
    justifyContent:'space-between',
  },
  listItemContent:{
    flexDirection:'column',
    justifyContent: 'center',
  },
  listItemOdd: {
    backgroundColor: red
  },
  listItemTitle: {
    fontWeight: "700",
    fontSize: 20
  },
  Even: {
    color: red
  },
  Odd: {
    color: white
  },
  BannerTitle: {
    fontSize: 42
  },
  Bold: {
    fontWeight: "700"
  }
});

export const SubBanner = StyleSheet.create({
  image: {
    flex: 1,
    width: null,
    height: null,
    flexDirection: "column"
  },
})

export const CounterStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: black,
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  button: {
    backgroundColor: red,
    marginTop: 50,
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    flex: 1
  },
  top: {
    flex: 3,
    flexDirection: "column"
  },
  bottom: {
    flexDirection: "row",
    justifyContent: "center",
    flex: 1
  }
});

export const Header = StyleSheet.create({
    header:{
        flex:0,
        justifyContent:'flex-start',
        flexDirection:'row',
        height:70
    },
  back: {
    width: 50,
    height: 50,
    backgroundColor: black,
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    margin: 10
  },
  title:{
      color:white,
      alignSelf:'center',
      flex:0,
      fontSize:20,
      marginLeft:20,
  }
});

export const Counter = StyleSheet.create({
  punch: {
    marginBottom: 5,
    width: 250,
    height: 250,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 250,
    backgroundColor: white,
  },
  counter: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  punchText: {
    fontSize: 150,
    color: "#0f0f0f"
  },
  techniqueText:{
      fontSize:25,
      color:white,
      marginTop:20,
  }
});
