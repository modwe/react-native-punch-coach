export const punches = [
  "One",
  "Two",
  "Three",
  "Four",
  "Five",
  "Six",
  "Seven",
  "Eight"
];
export const techniques = [
  "Left hook, head",
  "Right hook, head",
  "Left uppercut, chin",
  "Right uppercut, chin",
  "Right hook, body",
  "Left hook, body",
  "(Left) Jab, head",
  "Left uppercut, stomach"
];

export function getRandomPunch() {
  return Math.floor(Math.random() * Math.floor(punches.length));
}

export function getRandomSwitchingPunch(lastPunch) {
  if (lastPunch == null) {
    return getRandomSwitchingPunch(getRandomPunch());
  }

  let isEven = lastPunch % 2 == 0;

  if (isEven) {
    return getRandomEvenPunch();
  } else {
    return getRandomOddPunch();
  }
}

export function getRandomCombination(step) {
  let combination = [];

  let lastPunch = getRandomPunch();

  for (let i = 0; i < step; i++) {
    lastPunch = getRandomSwitchingPunch(lastPunch);
    combination.push(lastPunch);
  }

  return combination;
}

export const getRandomOddPunch = () => Math.floor(Math.random() * punches.length / 2) * 2;
export const getRandomEvenPunch = () => getRandomOddPunch() + 1;
