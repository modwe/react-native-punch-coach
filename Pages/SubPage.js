import React from "react";
import { Vibration } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";

import Menu from "../Components/Menu";
import SubBanner from "../Components/SubBanner";
import Container from "../Components/Container";

import { MenuStyles as styles } from "../styles";
import config from "../appconfig.json";

export default function SubPage({ navigation }) {
  //params from navigation
  const { params } = navigation.state;

  //navigation function
  const navigate = (route, params) => {
    navigation.navigate(route, params);
    Vibration.vibrate(config.vibration_duration);
  };

  //navigate back to previous page
  const navigateBack = () => {
    Vibration.vibrate(config.vibration_duration);
    navigation.goBack();
  };

  return (
    <Container>
      <Grid style={styles.content}>
        <Row size={50} style={styles.half}>
          <SubBanner
            action={navigateBack}
            image={params.image}
            title={params.title}
          />
        </Row>

        <Row size={50} style={styles.content}>
          <Col>
            <Menu items={params.items} navigate={navigate} />
          </Col>
        </Row>
      </Grid>
    </Container>
  );
};
