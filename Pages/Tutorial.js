import React from "react";
import { StyleSheet } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const styles = StyleSheet.create({
  image: {
    width: 320,
    height: 320
  }
});

//data for each slide
const slides = [
  {
    key: "prep",
    title: "Prepare the bag",
    text: "Mark the bag by this layout.",
    image: require("../Images/img1.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "select",
    title: "Select your mode.",
    text:
      "Select the technique you want to train.\nEvery session is 3 min long.",
    image: require("../Images/img2.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "listen",
    title: "Listen carefully",
    text: "Listen carefully which number is spoken.",
    image: require("../Images/img3.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p1",
    title: "One",
    text: 'When you hear "one"\n do a left hook to the head.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p2",
    title: "Two",
    text: 'When you hear "two"\n do a right hook to the head.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p3",
    title: "Three",
    text: 'When you hear "three"\n do a left uppercut to the chin.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p4",
    title: "Four",
    text: 'When you hear "four"\n do a right uppercut to the chin.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p5",
    title: "Five",
    text: 'When you hear "five"\n do a left hook to the body.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p6",
    title: "Six",
    text: 'When you hear "six"\n do a right hook to the body.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p7",
    title: "Seven",
    text: 'When you hear "seven"\n do a (left) jab to the head.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  },
  {
    key: "p8",
    title: "Eight",
    text: 'When you hear "eight"\n do a right uppercut to the stomach.',
    image: require("../Images/img4.jpg"),
    imageStyle: styles.image,
    backgroundColor: "#f44242"
  }
];

export default class Tutorial extends React.Component {
  //on 'Done' click event
  _onDone = () => {
    this.props.navigation.goBack();
  };
  render() {
    return <AppIntroSlider slides={slides} onDone={this._onDone} />;
  }
}
