import React, { Component } from "react";
import { Vibration } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import SplashScreen from "react-native-splash-screen";

import Container from "../Components/Container";
import Menu from "../Components/Menu";
import HomeBanner from "../Components/HomeBanner";
import items from "../menu.json";
import config from "../appconfig.json";
import { MenuStyles as styles } from "../styles";

//https://rationalappdev.com/layout-react-native-components-with-flexbox/

export default class Home extends Component {
  //navigate function prepared for menu
  navigate = (route, params) => {
    this.props.navigation.navigate(route, params);
    Vibration.vibrate(config.vibration_duration);
  };

  componentDidMount = () => {
    //splash screen hiding timeout, to disable white flickering
    setTimeout(() => {
      SplashScreen.hide(); 
    }, 250);
  };

  render() {
    return (
      <Container>
        <Grid style={styles.content}>
          <Row size={50} style={styles.half}>
            <HomeBanner />
          </Row>

          <Row size={50} style={styles.content}>
            <Col>
              <Menu items={items} navigate={this.navigate} />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}
