import React, { Component } from "react";
import {
  Text,
  View,
  Animated,
  TouchableOpacity,
  Vibration,
  BackHandler
} from "react-native";

import Tts from "react-native-tts";
import KeepAwake from "react-native-keep-awake";

import {
  punches,
  techniques,
  getRandomPunch,
  getRandomSwitchingPunch,
  getRandomCombination
} from "../Modules/Punches";
import { CounterStyles as styles } from "../styles";
import config from "../appconfig.json";

import Header from "../Components/Heder";
import Counter from "../Components/Counter";
import Container from "../Components/Container";
import SessionButton from "../Components/SessionButton";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animated: new Animated.Value(0),
      punch: null,
      interval: null,
      running: false,
      combination: null,
      technique: null
    };
    //handling device hw/sw backbutton
    this._didFocusSubscription = props.navigation.addListener(
      "didFocus",
      payload =>
        BackHandler.addEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  }

  //event for handlig the backbutton touch
  onBackButtonPressAndroid = () => {
    if (this.state.running) {
      return true;
    } else {
      Vibration.vibrate(config.vibration_duration);
      return false;
    }
  };

  //generates random number in range 0-2
  getRandomMode = () => {
    return Math.floor(Math.random() * 3);
  };

  //generates 2 or 3 randomly
  getRandomSteps = () => {
    let steps = [2, 3];
    let i = Math.round(Math.random());
    return steps[i];
  };

  //timeout function
  myTimeoutFunction = () => {
    //navigation params to read mode,time
    const { params } = this.props.navigation.state;
    
    //modes:
    //0 single punches
    //1 switchingpunches
    //2 combinations
    //3 random

    //if mode is 3 then there is a new mode generated in every iteration
    let mode = params.mode != 3 ? params.mode : this.getRandomMode();

    //switching modes
    switch (mode) {
      case 0: {
        let punch = getRandomPunch();
        Tts.speak(punches[punch]);
        this.setState({
          ...this.state,
          punch: punch,
          technique: techniques[punch],
          animated: new Animated.Value(0)
        });
        break;
      }
      case 1: {
        let punch = getRandomSwitchingPunch(this.state.punch);
        Tts.speak(punches[punch]);
        this.setState({
          ...this.state,
          punch: punch,
          technique: techniques[punch],
          animated: new Animated.Value(0)
        });
        break;
      }
      case 2: {
        let steps = params.mode != 3 ? params.steps : this.getRandomSteps();

        let combination = getRandomCombination(steps);

        //speaking every punch from the combination one by one
        //animatioan is handled in 'tts-finish' event for each punch except the first
        for (let i = 0; i < combination.length; i++) {
          Tts.speak(punches[combination[i]]);
        }

        this.setState({
          ...this.state,
          punch: combination[0],
          technique: techniques[combination[0]],
          combination: { punches: combination, first: true },
          animated: new Animated.Value(0)
        });
      }
    }

    //every session is 3min long
    if (Date.now() - (this.state.start + 180000) > 0) {
      this.stopInterval();
    }
  };

  startInterval = () => {
    let { params } = this.props.navigation.state;
    Tts.speak("Session started");
    this.setState({
      ...this.state,
      running: true,
      interval: setInterval(this.myTimeoutFunction, params.time),
      start: Date.now()
    });
  };

  stopInterval = () => {
    clearInterval(this.state.interval);
    this.setState({
      ...this.state,
      running: false,
      punch: null,
      combination: null,
      technique: null
    });
    Animated.timing(this.state.animated, {
      duration: 500,
      toValue: 0
    }).start();
    Tts.speak("Session Ended");
  };

  //toggle interval timer
  intervalAction = () => {
    if (this.state.running) {
      this.stopInterval();
    } else {
      this.startInterval();
    }
    Vibration.vibrate(config.vibration_duration);
  };

  componentDidMount = () => {
    //text to speach int + events
    Tts.getInitStatus().then(() => {
      Tts.setDucking(true);

      if (this.props.navigation.state.params.mode > 1) {
        Tts.setDefaultRate(0.8);
      } else {
        Tts.setDefaultRate(0.75);
      }

      Tts.addEventListener("tts-start", event => {
        if (this.state.running && this.state.punch != null) {
          Animated.timing(this.state.animated, {
            duration: 100,
            toValue: 1
          }).start();
        }
      });
      Tts.addEventListener("tts-finish", event => {
        if (
          this.state.combination != null &&
          this.state.combination.punches.length > 1
        ) {
          this.setState({
            ...this.state,
            combination: {
              punches: this.state.combination.punches.slice(1),
              first: false
            }
          });
          this.setState({
            ...this.state,
            punch: this.state.combination.punches[0],
            technique: techniques[this.state.combination.punches[0]]
          });
        }
        //  this.setState({...this.state,punch:this.state.combination.punches[0],technique:this.state.technique+'\n'+techniques[this.state.combination.punches[0]]});
      });
      Tts.addEventListener("tts-cancel", event => console.log("cancel", event));
    });


    this._willBlurSubscription = this.props.navigation.addListener(
      "willBlur",
      payload =>
        BackHandler.removeEventListener(
          "hardwareBackPress",
          this.onBackButtonPressAndroid
        )
    );
  };

  //handle back navigation
  goBack = () => {
    Vibration.vibrate(config.vibration_duration);
    this.props.navigation.goBack();
  };

  render() {
    let { params } = this.props.navigation.state;
    return (
      <Container>
        <View style={[styles.container]}>
          <View style={styles.top}>
            <KeepAwake />
            <Header
              visibleBack={!this.state.running}
              action={this.goBack}
              title={params.title}
            />
            <Counter
              animated={this.state.animated}
              punch={this.state.punch}
              technique={this.state.technique}
            />
          </View>

          <View style={styles.bottom}>
            <SessionButton
              action={this.intervalAction}
              running={this.state.running}
            />
          </View>
        </View>
      </Container>
    );
  }
}
