import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

import App from './Pages/App';
import Home from './Pages/Home';
import SubPage from './Pages/SubPage';
import Tutorial from './Pages/Tutorial';


const nav = StackNavigator({
        Home: {
          screen: Home,
        },
        App: {
          screen: App,
        },
        SubPage:{
          screen: SubPage,
        },
        Tutorial:{
          screen: Tutorial,
        }
      },
      {
        initialRouteName: 'Home',
        //disable native app header
        navigationOptions: {
          header: null,
        },
        //for iOS like animation on navigation
        transitionConfig: () => ({
          screenInterpolator: sceneProps => {
              const { layout, position, scene } = sceneProps;
              const { index } = scene;
  
              const translateX = position.interpolate({
                  inputRange: [index - 1, index, index + 1],
                  outputRange: [layout.initWidth, 0, 0]
              });
  
              const opacity = position.interpolate({
                  inputRange: [index - 1, index - 0.99, index, index + 0.99, index + 1],
                  outputRange: [0, 1, 1, 0.3, 0]
              });
  
              return { opacity, transform: [{ translateX }] }
          }
      })
  
  });

AppRegistry.registerComponent('BoxingTrainer', () => nav);
