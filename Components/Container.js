import React from "react";
import { View, StatusBar } from "react-native";

export default function Container({ children }) {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor="#f44242" barStyle="light-content" />
      {children}
    </View>
  );
}
