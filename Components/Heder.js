import React from "react";
import { View, Text } from "react-native";
import { Header as styles } from "../styles";

import Back from "./Back";

export default function Header({ action, visibleBack = true, title = "" }) {
  return (
    <View style={styles.header}>
      <Back action={action} isVisible={visibleBack} />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}
