import React from "react";
import { TouchableOpacity } from "react-native";

import { Header as styles } from "../styles";
import Icon from "react-native-vector-icons/Feather";

export default function Back({ isVisible, action }) {
  if (isVisible) {
    return (
      <TouchableOpacity style={styles.back} onPress={action}>
        <Icon name="chevron-left" size={30} color="#fff" />
      </TouchableOpacity>
    );
  }
  return null;
}
