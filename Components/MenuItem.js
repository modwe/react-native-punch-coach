import React from "react";
import { MenuStyles as styles } from "../styles";
import { Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/Feather";

export default function MenuItem({ e, i, navigate }) {
  return (
    <TouchableOpacity
      style={[styles.listItem, e.id % 2 == 0 ? styles.listItemOdd : null]}
      onPress={() => navigate(e.route, e.params)}
    >
      <View>
        <Text
          style={[
            styles.listItemTitle,
            e.id % 2 == 0 ? styles.Odd : styles.Even
          ]}
        >
          {e.title}
        </Text>
        <Text style={e.id % 2 == 0 ? styles.Odd : styles.Even}>
          {e.subtitle}
        </Text>
      </View>
      <View>
        <Icon
          name="chevron-right"
          size={30}
          color={e.id % 2 == 0 ? "#fff" : "#f44242"}
        />
      </View>
    </TouchableOpacity>
  );
}
