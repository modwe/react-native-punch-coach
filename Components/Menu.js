import React from "react";
import MenuItem from "./MenuItem";

export default function Menu({ items, navigate }) {
  return items.map((e, i) => {
    return <MenuItem key={e.id} e={e} i={i} navigate={navigate} />;
  });
}
