import React from "react";
import { View, Animated, Text } from "react-native";

import { Counter as styles } from "../styles";

export default function Counter({ animated, punch, technique }) {
  return (
    <View style={styles.counter}>
      <Animated.View
        style={[
          styles.punch,
          {
            opacity: animated,
            transform: [
              {
                scale: animated
              }
            ]
          }
        ]}
      >
        <Text style={styles.punchText}>{punch != null ? punch + 1 : null}</Text>
      </Animated.View>
      <Text style={styles.techniqueText}>
        {technique != null ? technique : null}
      </Text>
    </View>
  );
}
