import React from "react";

import { MenuStyles as styles } from "../styles";
import { ImageBackground, View, Text } from "react-native";
import pack from "../package.json";

export default function HimeBanner() {
  return (
    <ImageBackground
      resizeMode="cover"
      style={styles.image}
      source={require("../Images/Home2.jpg")}
    >
      <View style={styles.horizontal}>
        <Text style={[styles.BannerTitle, styles.Bold, styles.Even]}>
          Punch{" "}
        </Text>
        <Text style={[styles.BannerTitle, styles.Odd]}>Coach</Text>
      </View>
      <View style={[styles.horizontal]}>
        <Text style={[styles.Odd]}>{`V ${pack.version}`}</Text>
      </View>
    </ImageBackground>
  );
}
