import React, { Component } from "react";
import { MenuStyles as styles } from "../styles";
import { ImageBackground, View, Text } from "react-native";
import Header from "./Heder";

export default function SubBanner({ image, title, action }) {
  const getImage = () => {
    switch (image) {
      case 0: {
        return require("../Images/Single.jpg");
      }
      case 1: {
        return require("../Images/Combination.jpg");
      }
      default: {
        return require("../Images/Home.jpg");
      }
    }
  };

  return (
    <ImageBackground
      resizeMode="cover"
      style={styles.image}
      source={getImage()}
    >
      <View style={styles.header}>
        <Header action={action} />
      </View>
      <View style={styles.horizontal}>
        <Text style={[styles.BannerTitle, styles.Bold, styles.Even]}>
          {title}
        </Text>
      </View>
    </ImageBackground>
  );
}
