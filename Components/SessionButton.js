import React from "react";
import { TouchableOpacity, Text } from "react-native";

import { CounterStyles as styles } from "../styles";

export default function SessionButton({ running, action }) {
  return (
    <TouchableOpacity style={styles.button} onLongPress={action}>
      <Text
        style={{ textAlign: "center", color: "#fff", margin: 15, fontSize: 25 }}
      >
        {running ? "Hold to stop" : "Hold to start"}
      </Text>
    </TouchableOpacity>
  );
}
